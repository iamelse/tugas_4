<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyC extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['title']  = 'Perusahaan';
        $company        = Company::all();
        return view('companies.index', ['company' => $company], $data);
    }

    private function _validate(Request $request){
        $validatedData = $request->validate([
            'nama'   => 'required|min:2|max:50',
            'alamat' => 'required|min:2|max:50'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['title']  = 'Perusahaan - Tambah Perusahaan';
        return view('companies.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $company = $request->all();
        $this->_validate($request);

        Company::create($company);
        return redirect()->route('companies.index')->with('success','Data Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['title']  = 'Perusahaan - Edit Data Perusahaan';
        $company        = Company::find($id);
        return view('companies.edit', ['company' => $company], $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->_validate($request);
        $company = Company::find($id);
        $company->update($request->all());
        return redirect()->route('companies.index')->with('success','Data Berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $company = Company::find($id);
        $company->delete();
        return redirect()->route('companies.index')->with('success','Data Berhasil dihapus');
    }
}
