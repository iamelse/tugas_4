<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $fillable = [
        'nama',
        'alamat'
    ];
    public function employee(){
        return $this->hasOne('\App\Models\Employee','company_id');
    }

}
